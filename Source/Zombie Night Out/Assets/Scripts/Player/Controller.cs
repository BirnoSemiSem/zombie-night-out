﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    [SerializeField] private Rigidbody _rb;
    [SerializeField] private Vector3 TargetMove;

    [SerializeField] private float speed;
    // Start is called before the first frame update
    void Start()
    {
        if (_rb == null)
            _rb = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D))
        {
            float movengX = Input.GetAxis("Horizontal");
            float movengZ = Input.GetAxis("Vertical");

            Vector3 movement = new Vector3(movengX > 0 || movengX < 0 ? movengX : 0, 0, movengZ > 0 || movengZ < 0 ? movengZ : 0);

            TargetMove = movement;
        }
        else
            TargetMove = new Vector3(0, 0, 0);

        if(TargetMove != Vector3.zero)
        {
            _rb.MovePosition(_rb.position + (TargetMove * speed * Time.deltaTime));

            Look(TargetMove, 5f);
        }
    }

    public void Look(Vector3 Target, float speed)
    {
        float angle = Mathf.Atan2(Target.x, Target.z) * Mathf.Rad2Deg;
        angle = -angle;

        //transform.rotation = Quaternion.Euler(new Vector3(90, 0, angle));
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(90, 0, angle)), speed * Time.deltaTime);
    }
}
