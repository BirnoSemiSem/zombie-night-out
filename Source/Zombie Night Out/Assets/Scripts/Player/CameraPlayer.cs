﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPlayer : MonoBehaviour
{
    public float dumping = 2f; // сглаживание камеры. скольжение
    public Vector3 offset = new Vector3(2f,0f, 10f); //смещение камеры относительно от персонажа. x y
    public GameObject Target; // положение персонажа

    [SerializeField]
    float limitleft;
    [SerializeField]
    float limitright;
    [SerializeField]
    float limitbottom;
    [SerializeField]
    float limitupper;

    public bool DebugCamera = false;


    void Start()
    {
        offset = new Vector3(Mathf.Abs(offset.x),0f, offset.z); // вычесление смещения камеры
        //transform.position = new Vector3(player.position.x + offset.x, player.position.y + offset.y, transform.position.z);
        //CameraForAnglePlayer();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Target)
        {
            CameraForAnglePlayer();

            Vector3 target;
            target = new Vector3(Target.transform.position.x + offset.x, transform.position.y, Target.transform.position.z + offset.z);

            Vector3 currentPosition = Vector3.Lerp(transform.position, target, dumping * Time.deltaTime);
            transform.position = currentPosition;
        }

        //transform.position = new Vector3(
        //Mathf.Clamp(transform.position.x, limitleft, limitright),
        //Mathf.Clamp(transform.position.y, limitbottom, limitupper),
        //transform.position.z);
    }

    void CameraForAnglePlayer()
    {
        //Debug.Log("player.eulerAngles.z: " + Target.transform.localRotation.eulerAngles.z);
        if (!DebugCamera)
        {
            //if (Target.transform.eulerAngles.z >= 0 && Target.transform.eulerAngles.z <= 15)
            //    offset = new Vector2(0, 0.5f);
            //else if (Target.transform.eulerAngles.z > 15 && Target.transform.eulerAngles.z <= 75)
            //    offset = new Vector2(-1, 0.5f);
            //else if (Target.transform.eulerAngles.z > 75 && Target.transform.eulerAngles.z <= 105)
            //    offset = new Vector2(-1, 0);
            //else if (Target.transform.eulerAngles.z > 105 && Target.transform.eulerAngles.z <= 165)
            //    offset = new Vector2(-1, -0.5f);
            //else if (Target.transform.eulerAngles.z > 165 && Target.transform.eulerAngles.z <= 195)
            //    offset = new Vector2(0, -0.5f);
            //else if (Target.transform.eulerAngles.z > 195 && Target.transform.eulerAngles.z <= 255)
            //    offset = new Vector2(1, -0.5f);
            //else if (Target.transform.eulerAngles.z > 255 && Target.transform.eulerAngles.z <= 285)
            //    offset = new Vector2(1, 0);
            //else if (Target.transform.eulerAngles.z > 285 && Target.transform.eulerAngles.z <= 345)
            //    offset = new Vector2(1, 0.5f);
            //else if (Target.transform.eulerAngles.z > 345)
            //    offset = new Vector2(0, 0.5f);

            float x, y, temp;
            x = y = 0;

            if (Target.transform.localRotation.eulerAngles.z < 90)
                temp = Target.transform.localRotation.eulerAngles.z / 90;
            else if (Target.transform.localRotation.eulerAngles.z < 180)
                temp = (Target.transform.localRotation.eulerAngles.z - 90) / 90;
            else if (Target.transform.localRotation.eulerAngles.z < 270)
                temp = (Target.transform.localRotation.eulerAngles.z - 180) / 90;
            else if (Target.transform.localRotation.eulerAngles.z < 360)
                temp = (Target.transform.localRotation.eulerAngles.z - 270) / 90;
            else
                temp = 0;

            if (Target.transform.localRotation.eulerAngles.z < 90 || Target.transform.localRotation.eulerAngles.z > 180)
                x = temp;
            else if ((Target.transform.localRotation.eulerAngles.z >= 90 && Target.transform.localRotation.eulerAngles.z < 180) || Target.transform.localRotation.eulerAngles.z > 270)
                x = 1 - temp;

            if ((Target.transform.localRotation.eulerAngles.z >= 90 && Target.transform.localRotation.eulerAngles.z < 180) || Target.transform.localRotation.eulerAngles.z > 270)
                y = temp;
            else
                y = 1 - temp;

            if (Target.transform.localRotation.eulerAngles.z < 180)
                x *= -1;

            if (Target.transform.localRotation.eulerAngles.z >= 90 && Target.transform.localRotation.eulerAngles.z < 270)
                y *= -1;

            offset = new Vector3(x, 0f, y);
        }
    }
}