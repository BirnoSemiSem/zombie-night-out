﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Bot : MonoBehaviour
{
    [SerializeField] private NavMeshAgent _agent;

    [SerializeField] private GameObject Target;
    [SerializeField] private Vector3 Point;

    [SerializeField] private bool BotIsFolow = false;

    [SerializeField] private float MinRandom = 0.1f;
    [SerializeField] private float MaxRandom = 1f;


    [SerializeField] private bool DebugIsFollow = false;
    // Start is called before the first frame update
    void Start()
    {
        _agent.updateRotation = false;
        _agent.updateUpAxis = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (DebugIsFollow)
        {
            if (BotIsFolow)
            {
                if (Point != Vector3.zero && Vector3.Distance(Point, Target.transform.position) > MaxRandom)
                {
                    Point = RandomBoxPoint(Target.transform.position, MinRandom, MaxRandom);
                    _agent.SetDestination(Point);
                }
                else if (Vector3.Distance(Point, transform.position) < 0.5f)
                {
                    Point = Vector3.zero;
                    BotWalkStop();
                    BotIsFolow = false;
                    Debug.Log("Бот: Я на месте");
                }
            }
            else
            {
                Point = RandomBoxPoint(Target.transform.position, MinRandom, MaxRandom);
                _agent.SetDestination(Point);
                BotIsFolow = true;
                Debug.Log("Бот: Бегу за игроком");
            }
        }
        else
            BotWalkStop();

        if(BotIsFolow)
        {
            Debug.DrawLine(new Vector3(Target.transform.position.x - MaxRandom, Point.y, Target.transform.position.z + MaxRandom), new Vector3(Target.transform.position.x + MaxRandom, Point.y, Target.transform.position.z + MaxRandom), Color.red);
            Debug.DrawLine(new Vector3(Target.transform.position.x - MaxRandom, Point.y, Target.transform.position.z - MaxRandom), new Vector3(Target.transform.position.x + MaxRandom, Point.y, Target.transform.position.z - MaxRandom), Color.red);
            Debug.DrawLine(new Vector3(Target.transform.position.x - MaxRandom, Point.y, Target.transform.position.z - MaxRandom), new Vector3(Target.transform.position.x - MaxRandom, Point.y, Target.transform.position.z + MaxRandom), Color.red);
            Debug.DrawLine(new Vector3(Target.transform.position.x + MaxRandom, Point.y, Target.transform.position.z - MaxRandom), new Vector3(Target.transform.position.x + MaxRandom, Point.y, Target.transform.position.z + MaxRandom), Color.red);

            Debug.DrawLine(Point, Point + new Vector3(0,0,0.1f), Color.yellow);
        }
    }

    public void BotWalkStop()
    {
        if (!_agent.isOnNavMesh)
            return;

        _agent.ResetPath();
    }

    Vector3 RandomBoxPoint(Vector3 Point, float MinR, float MaxR)
    {
        float RandomX = Random.Range(1, 10) > 5 ? (-1 * Random.Range(MinR, MaxR)) : Random.Range(MinR, MaxR);
        float RandomZ = Random.Range(1, 10) > 5 ? (-1 * Random.Range(MinR, MaxR)) : Random.Range(MinR, MaxR);

        Vector3 temp = new Vector3(Point.x + RandomX,
            Point.y,
            Point.z + RandomZ);

        return temp;
    }
}
